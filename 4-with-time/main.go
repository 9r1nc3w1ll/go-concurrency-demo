package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func main() {
	// Adds two go routines to the waitgroup
	wg.Add(2)

	go foo()
	go bar()

	// Wait group waits
	wg.Wait()
}

func foo() {
	for i := 0; i < 20; i++ {
		fmt.Println("foo:", i+1)
		time.Sleep(time.Duration(30 * time.Millisecond))
	}
	wg.Done()
}

func bar() {
	for i := 0; i < 20; i++ {
		fmt.Println("bar:", i+1)
		time.Sleep(time.Duration(30 * time.Millisecond))
	}
	wg.Done()
}
