package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var wg sync.WaitGroup
var counter int
var mutex sync.Mutex

func main() {
	// Adds two go routines to the waitgroup
	wg.Add(2)

	go increment("foo")
	go increment("bar")

	// Wait group waits
	wg.Wait()
	fmt.Println("\nfinal counter:", counter)
}

func increment(name string) {
	for i := 0; i < 20; i++ {
		time.Sleep(time.Duration(rand.Intn(2)) * time.Millisecond)
		mutex.Lock()
		counter++
		fmt.Println(name, i+1, "counter:", counter)
		mutex.Unlock()
	}
	wg.Done()
}

// Check for race condition
